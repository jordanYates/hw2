#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#define MAX_DAYS          365
#define MAX_STATS         100
#define MAX_STATION_COUNT 20

#include "stat.h"
#include "day.h"
#include "station.h"
#include "region.h"
#include "analyzer.h"

int main(int argc, char* argv[]) {
	if (argc>1) {
		std::ifstream inputStream(argv[1], std::ios_base::in);

		Region region;
		region.load(inputStream);

		Analyzer analyzer;

		if (argc>2) {
			std::string stationId(argv[2]);
			Station* station = region.findStation(stationId);
			if (station!= nullptr)
				analyzer.analyze(station);
		}
		else {
			region.resetStationIteration();
			Station *station;
			while ((station = region.getNextStation()) != nullptr)
				analyzer.analyze(station);
		}
	}
	else {
		std::cout << "You need to select what data you would like displaed. Run the program again and enter a reference to\n the data you would like to see so it looks like the following: Data/2013-Jan-to-Mar.csv." << std::endl;
		std::cout << "If you would like to see the data for a specific station put a space after the csv name and type in\n COOP: then the station number." << std::endl;
	}
	
	return 0;
}
