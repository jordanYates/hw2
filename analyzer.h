#ifndef ANALYZER_H 
#define ANALYZER_H

#include "station.h"

class Analyzer {
	public:
		void analyze(Station* station);
};

#endif